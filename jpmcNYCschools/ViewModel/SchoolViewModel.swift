//
//  SchoolViewModel'.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import Foundation

class SchoolViewModel: ObservableObject {
    @Published var schools: [School] = []
    @Published var sats: [SatScore] = []

    let dataManager: DataManager

    init(dataManager: DataManager) {
        self.dataManager = dataManager
        getSchools()
        getSats()
    }

    func getSats() {
        dataManager.getDecodableFromURL(urlString: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") { sats in
            self.sats = sats
        }
    }
    
    func getSchools() {
        dataManager.getDecodableFromURL(urlString: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") { schools in
            self.schools = schools
        }
    }
}
