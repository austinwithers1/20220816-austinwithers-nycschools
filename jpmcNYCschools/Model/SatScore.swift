//
//  SatScore.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import Foundation

struct SatScore: Codable {

    let id : String
    let readingRaw: String
    let mathRaw: String
    let writingRaw: String
    var readingScore: Int {
        return Int(readingRaw) ?? 0
    }
    var mathScore: Int {
        return Int(mathRaw) ?? 0
    }
    var writingScore: Int {
        return Int(writingRaw) ?? 0
    }


    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case readingRaw = "sat_critical_reading_avg_score"
        case mathRaw = "sat_math_avg_score"
        case writingRaw = "sat_writing_avg_score"
    }
}
