//
//  School.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import Foundation

struct School: Codable, Identifiable {
    let id : String
    var name : String
    var address : String
    var phone : String
    var email : String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case phone = "phone_number"
        case address = "location"
        case email = "school_email"
    }
}
