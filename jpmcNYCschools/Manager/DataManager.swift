//
//  DataManagerProtocol.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import Foundation

protocol DataManager {

    func getDecodableFromURL<T: Decodable>(urlString: String, completion: @escaping (T) -> ())
    
}
