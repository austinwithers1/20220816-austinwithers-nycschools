//
//  NetworkManager.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import Foundation

class NetworkManager: DataManager {
    public static let shared = NetworkManager()

    func getDecodableFromURL<T: Decodable>(urlString: String, completion: @escaping (T) -> ()) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data {
                let data = try? JSONDecoder().decode(T.self, from: data)

                if let data {
                    DispatchQueue.main.async {
                        completion(data)
                    }
                }
            }

        }
        .resume()
    }
}
