//
//  SchoolView.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import SwiftUI

struct SchoolView: View {

    var school: School

    var body: some View {

        VStack {
            Text(school.name)
                .font(.headline)
                .frame(maxWidth: .infinity, alignment: .leading)
            VStack {

                Text(school.phone)
                    .font(.footnote)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .foregroundColor(.cyan)
                    .background(.regularMaterial)
                if school.email != nil {
                    Text(school.email!)
                        .font(.footnote)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .foregroundColor(.cyan)
                        .background(.regularMaterial)

                }
            }
        }
    }
}

struct SchoolView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolView(school: School(id: "", name: "High School", address: "", phone: "838 993 8293", email: nil))
    }
}
