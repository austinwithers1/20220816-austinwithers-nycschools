//
//  SatScoreView.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import SwiftUI

struct SatScoreView: View {

    var satScore: SatScore

    var body: some View {
        HStack {

            Divider()
                .frame(width: 5)
                .background(.cyan)
            VStack {

                Text("Average Scores")
                    .font(.headline)
                HStack {
                    VStack {
                        Text("Reading")
                        Text("\(satScore.readingScore)")
                    }
                    VStack {
                        Text("Math")
                        Text("\(satScore.mathScore)")
                    }
                    VStack {
                        Text("Writing")
                        Text("\(satScore.writingScore)")
                    }
                }
                .frame(maxWidth: .infinity)
            }
        }
        .background(.thinMaterial)
    }
}
struct SatScoreView_Previews: PreviewProvider {
    static var previews: some View {
        SatScoreView(satScore: SatScore(id: "", readingRaw: "440", mathRaw: "300", writingRaw: "303"))
    }
}
