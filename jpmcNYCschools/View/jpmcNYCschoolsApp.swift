//
//  jpmcNYCschoolsApp.swift
//  jpmcNYCschools
//
//  Created by Consultant  on 8/16/22.
//

import SwiftUI

@main
struct jpmcNYCschoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView(viewModel: SchoolViewModel(dataManager: NetworkManager.shared))
        }
    }
}
