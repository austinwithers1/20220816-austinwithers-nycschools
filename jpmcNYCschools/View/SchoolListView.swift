//
//  ContentView.swift
//  jpmcNYCschools
//
//  Created by Austin Withers  on 8/16/22.
//

import SwiftUI

struct SchoolListView: View {
    @StateObject var viewModel: SchoolViewModel

    @State private var expanded: [String] = []

    var body: some View {
        VStack {
            Text("New York Schools")
                .font(.largeTitle)
                .padding()
            List(viewModel.schools) { school in
                VStack {
                    HStack {
                        Divider()
                            .frame(width: 5)
                            .background(.orange)
                        SchoolView(school: school)
                        if viewModel.sats.contains(where: {$0.id == school.id }) {
                            if expanded.contains(school.id) {
                                Image(systemName: "arrow.up.circle")
                                    .onTapGesture {
                                        expanded.removeAll(where: {$0 == school.id})
                                    }
                            }

                            if !expanded.contains(school.id) {
                                Image(systemName: "arrow.down.circle")
                                    .onTapGesture {
                                        expanded.append(school.id)
                                    }
                            }
                        }
                    }
                    if expanded.contains(school.id) {
                        let satScore = viewModel.sats.first(where: {$0.id == school.id})
                        SatScoreView(satScore: satScore!)
                    }
                }
            }
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView(viewModel: SchoolViewModel(dataManager: NetworkManager.shared))
    }
}
